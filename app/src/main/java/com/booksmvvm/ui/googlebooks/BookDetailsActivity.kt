package com.booksmvvm.ui.googlebooks

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.booksmvvm.R
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_bookdetails.*

class BookDetailsActivity : AppCompatActivity(){
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_bookdetails)

        val bundle = intent.extras
        val authors = bundle?.get("authors")
        val title = bundle?.get("title")
        val desc = bundle?.get("desc")
        val publishDate = bundle?.get("publishDate")
        val publisher = bundle?.get("publisher")
        val pages = bundle?.get("pages")
        val rating = bundle?.get("rating")
        val image = bundle?.get("image")

        AuthorsTV.text = (authors as CharSequence?)
        TitleTV.text = (title as CharSequence?)
        DescriptionTV.text = (desc as CharSequence?)
        PublisherTV.text = (publisher as CharSequence?)
        PublishDateTV.text = (publishDate as CharSequence?)
        PagesTV.text = (pages as CharSequence?)

        Glide.with(this)
            .load(image)
            .into(ThumbnailIV)
    }
}
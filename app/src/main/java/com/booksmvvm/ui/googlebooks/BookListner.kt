package com.booksmvvm.ui.googlebooks

import com.booksmvvm.data.googlebooks.models.Volume

interface BookListner {
    fun onSuccess (books: List<Volume>)
    fun onError()
}
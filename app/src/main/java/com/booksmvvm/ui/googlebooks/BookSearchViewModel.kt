package com.booksmvvm.ui.googlebooks

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.view.View
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.booksmvvm.data.firebase.FirebaseSource
import com.booksmvvm.data.googlebooks.models.VolumesResponse
import com.booksmvvm.data.repositories.BookRepository
import com.booksmvvm.data.repositories.UserRepository
import com.booksmvvm.ui.auth.LoginActivity
import com.booksmvvm.utils.startLoginActivity

class BookSearchViewModel()  : ViewModel(){

    private val firebase: FirebaseSource = FirebaseSource()
    private val repository: BookRepository = BookRepository()
    private val userRepository: UserRepository = UserRepository(firebase)
    private val volumesResponseLiveData: LiveData<VolumesResponse> = repository.getVolumesResponseLiveData()

    fun searchVolumes(keyword: String) {
        repository.searchVolumes(keyword)
    }

    fun getVolumesResponseLiveData(): LiveData<VolumesResponse> {
        return volumesResponseLiveData
    }

    fun logout(context: Context){
        userRepository.logout()
        val intent = Intent(context, LoginActivity::class.java)
        context.startActivity(intent)
    }
}
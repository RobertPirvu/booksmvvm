package com.booksmvvm.ui.googlebooks

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.booksmvvm.data.repositories.BookRepository
import com.booksmvvm.ui.home.HomeActivity

class BookSearchViewModelFactory() : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if(modelClass.isAssignableFrom(BookSearchViewModel::class.java)){
            return BookSearchViewModel() as T
        }
        throw IllegalArgumentException ("UnknownViewModel")
    }

}

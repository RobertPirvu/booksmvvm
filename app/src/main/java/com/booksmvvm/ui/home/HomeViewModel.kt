package com.booksmvvm.ui.home

import android.view.View
import androidx.lifecycle.ViewModel
import com.booksmvvm.data.repositories.UserRepository
import com.booksmvvm.utils.startLoginActivity

class HomeViewModel(
    private val repository: UserRepository
) : ViewModel() {

    var title: String? = null

    val user by lazy {
        repository.currentUser()
    }
    
    fun logout(view: View){
        repository.logout()
        view.context.startLoginActivity()
    }
}
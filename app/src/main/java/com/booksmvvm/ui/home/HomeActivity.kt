package com.booksmvvm.ui.home

import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.booksmvvm.R
import com.booksmvvm.data.adapter.BookSearchAdapter
import com.booksmvvm.data.googlebooks.models.Volume
import com.booksmvvm.ui.googlebooks.BookSearchViewModel
import com.booksmvvm.ui.googlebooks.BookSearchViewModelFactory
import kotlinx.android.synthetic.main.activity_home.*


class HomeActivity() : AppCompatActivity() {
    private lateinit var toggle: ActionBarDrawerToggle
    private lateinit var viewModel: BookSearchViewModel
    private lateinit var adapter: BookSearchAdapter
    private lateinit var factory : BookSearchViewModelFactory
    private val mlayoutManager: LinearLayoutManager = LinearLayoutManager(this)
    private var bookList: ArrayList<Volume> = ArrayList()
//    private val view : View = View(this)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

    toggle = ActionBarDrawerToggle(this, drawerLayout, R.string.open, R.string.close)
    drawerLayout.addDrawerListener(toggle)
    toggle.syncState()

    supportActionBar?.setDisplayHomeAsUpEnabled(true)
    navView.setNavigationItemSelectedListener {
        when(it.itemId) {
            R.id.nav_logout -> {
                    viewModel.logout(this)
                Toast.makeText(this, "Logout", Toast.LENGTH_SHORT).show()
            }
            R.id.nav_favorites -> Toast.makeText(this, "Logout", Toast.LENGTH_SHORT).show()
        }
        true
    }

        recycler_view_id.setHasFixedSize(true)

        factory = BookSearchViewModelFactory()
        viewModel = ViewModelProvider(this, factory).get(BookSearchViewModel::class.java)



        search()
        initAdapter()
        observeData()
    }

    private fun search(){
        search_button.setOnClickListener {
            Log.d("ars", "onStart")
            if (keyword_search_term.text.isEmpty()) {
                Toast.makeText(it.context, "enter the book", Toast.LENGTH_SHORT)
                    .show()
            } else {
                viewModel.searchVolumes(keyword_search_term.text.toString())
            }
        }
    }

    private fun observeData() {
        viewModel.getVolumesResponseLiveData().observe(this, Observer { volumesResponse ->
            if (volumesResponse.totalItems > 0) {
                adapter.setResults(volumesResponse.items)
//                volumesResponse.items.clear()
            } else {
                Toast.makeText(this, "No result found! Please try another search term!", Toast.LENGTH_SHORT)
                    .show()
            }
        })
    }
    private fun initAdapter(){
        adapter = BookSearchAdapter(this, bookList )
        recycler_view_id.layoutManager = mlayoutManager
        recycler_view_id.adapter = adapter
    }


}

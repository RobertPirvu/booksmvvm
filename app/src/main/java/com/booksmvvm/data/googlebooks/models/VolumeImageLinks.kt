package com.booksmvvm.data.googlebooks.models

import com.google.gson.annotations.SerializedName

data class VolumeImageLinks(
    @SerializedName("smallThumbnail")
    val smallThumbnail: String,

    @SerializedName("thumbnail")
    val thumbnail: String
) {
    @JvmName("getSmallThumbnail1")
    fun getSmallThumbnail(): String? {
        return smallThumbnail
    }
}
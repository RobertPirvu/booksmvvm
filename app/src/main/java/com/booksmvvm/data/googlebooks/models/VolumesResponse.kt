package com.booksmvvm.data.googlebooks.models

data class VolumesResponse(
    val kind: String,
    val totalItems: Int,
    val items: ArrayList<Volume>
)
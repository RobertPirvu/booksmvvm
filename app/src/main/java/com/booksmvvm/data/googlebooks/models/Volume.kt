package com.booksmvvm.data.googlebooks.models

import com.google.gson.annotations.SerializedName

data class Volume(

    @SerializedName("volumeInfo")
    val volumeInfo: VolumeInfo,
    @SerializedName("kind")
    val kind: String,
    @SerializedName("id")
    val id: String,
    @SerializedName("etag")
    val etag: String,
    @SerializedName("selfLink")
    val selfLink: String


) {

    @JvmName("getVolumeInfo1")
    fun getVolumeInfo(): VolumeInfo? {
        return volumeInfo
    }
}
package com.booksmvvm.data

import com.booksmvvm.data.googlebooks.models.VolumesResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface RetrofitServieces {
    @GET("books/v1/volumes")
    fun getBookList(@Query("q")title: String): Call<VolumesResponse>

}

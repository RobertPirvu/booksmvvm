package com.booksmvvm.data.googlebooks.api

import com.booksmvvm.data.RetrofitServieces

object Common {
    private const val BASE_URL = "https://www.googleapis.com/"
    val retrofitService: RetrofitServieces
        get() = RetrofitClient.getClient(BASE_URL).create(RetrofitServieces::class.java)
}
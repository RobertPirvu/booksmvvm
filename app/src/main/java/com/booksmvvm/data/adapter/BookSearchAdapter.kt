package com.booksmvvm.data.adapter

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.booksmvvm.R
import com.booksmvvm.data.googlebooks.models.Volume
import com.booksmvvm.ui.googlebooks.BookDetailsActivity
import com.booksmvvm.ui.home.HomeActivity
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.book_item.view.*


class BookSearchAdapter(private val context: Activity, private var bookList: ArrayList<Volume>) :


    RecyclerView.Adapter<BookSearchAdapter.MyViewHolder>() {

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val image: ImageView = itemView.poster
        val bookTitle: TextView = itemView.book_name
        val authors: TextView = itemView.author
        val year: TextView = itemView.year
        val pages: TextView = itemView.PagesTV
        val rating: TextView = itemView.rating
        val info: RelativeLayout = itemView.reltivCard

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView =
            LayoutInflater.from(parent.context).inflate(R.layout.book_item, parent, false)
        return MyViewHolder(itemView)
    }

    override fun getItemCount() = bookList.size

    fun setResults(bookList: ArrayList<Volume>) {
        this.bookList.clear()
        this.bookList.addAll(bookList)
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {

        val book = bookList[position]
//        val volume: Volume = bookList[position]

        var auth: String = ""
        try {
            for (i in book.volumeInfo.authors) {
                auth += i + "\n"
            }
        }
        catch (e: NullPointerException) {

        }

        if (book.getVolumeInfo()!!.getImageLinks() != null) {
            val imageUrl = book.getVolumeInfo()!!.getImageLinks()!!.getSmallThumbnail()!!.replace("http://", "https://")
            Glide.with(holder.itemView)
                .load(imageUrl)
                .into(holder.image)
        }

        holder.bookTitle.text = book.volumeInfo.bookName;
        holder.year.text = book.volumeInfo.publishedDate
        holder.authors.text = auth
        holder.pages.text = book.volumeInfo.pageCount.toString()
        holder.rating.text = book.volumeInfo.ratingsCount.toString()

        holder.info.setOnClickListener {

            val toPass = Bundle()
            toPass.putString("authors", auth)
            toPass.putString("title", book.volumeInfo.bookName)
            toPass.putString("desc", book.volumeInfo.description)
            toPass.putString("publishDate", book.volumeInfo.publishedDate)
            toPass.putString("publisher", book.volumeInfo.publisher)
            toPass.putString("pages", book.volumeInfo.pageCount.toString())
            toPass.putString("rating", book.volumeInfo.ratingsCount.toString())
            toPass.putString("image", book.getVolumeInfo()!!.getImageLinks()!!.getSmallThumbnail()!!.replace("http://", "https://"))


            val intent = Intent(context, BookDetailsActivity::class.java)
            intent.putExtras(toPass)
            context.startActivity(intent)
        }
    }


}


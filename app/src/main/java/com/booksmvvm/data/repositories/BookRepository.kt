package com.booksmvvm.data.repositories

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.booksmvvm.data.googlebooks.api.Common
import com.booksmvvm.data.RetrofitServieces
import com.booksmvvm.data.googlebooks.models.VolumesResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class BookRepository {
    private val mService: RetrofitServieces = Common.retrofitService
    private val volumesResponseLiveData:MutableLiveData<VolumesResponse> = MutableLiveData()

    fun BookRepository() {}

    fun searchVolumes(keyword: String) {

        mService.getBookList(keyword).enqueue(object : Callback<VolumesResponse> {
            override fun onResponse(
                call: Call<VolumesResponse>,
                response: Response<VolumesResponse>
            ) {
                Log.d("ars", "onResponse")
                if (response.body() != null){
                    volumesResponseLiveData.postValue(response.body());
                }
            }

            override fun onFailure(call: Call<VolumesResponse>, t: Throwable) {
                Log.d("ars", "onFailure")
                volumesResponseLiveData.postValue(null);
            }
        })
    }

    fun getVolumesResponseLiveData() : LiveData<VolumesResponse>{
        return volumesResponseLiveData;
    }
}
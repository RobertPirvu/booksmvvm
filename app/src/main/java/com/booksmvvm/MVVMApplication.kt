package com.booksmvvm

import android.app.Application
import com.booksmvvm.data.firebase.FirebaseSource
import com.booksmvvm.data.repositories.UserRepository
import com.booksmvvm.ui.auth.AuthViewModelFactory
import com.booksmvvm.ui.googlebooks.BookSearchViewModelFactory
import com.booksmvvm.ui.home.HomeViewModelFactory
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.androidXModule
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.provider
import org.kodein.di.generic.singleton

class MVVMApplication : Application(), KodeinAware{

    override val kodein = Kodein.lazy {
        import(androidXModule(this@MVVMApplication))

        bind() from singleton { FirebaseSource() }
        bind() from singleton { UserRepository(instance()) }
        bind() from provider { AuthViewModelFactory(instance()) }


    }
}